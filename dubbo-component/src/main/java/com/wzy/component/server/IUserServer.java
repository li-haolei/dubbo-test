package com.wzy.component.server;

import com.wzy.component.entity.User;

import java.util.List;

public interface IUserServer {
    public User getById(long id);
    public List<User> getAll();
    public List<User> getAll2();
    public List<User> getAll3();
    public List<User> getAll4();

    /**
     * 测试过程:
     *  生产者和消费者都依赖了component的jar包
     *  1.先install component完成引用
     *  2.添加getall5
     *  3.单独打开product子项目  无报错 也未更新
     *  4.再次install component 本地jar包更新，子项目重新打开类发现提示要重写了。
     *  5.实际工作中要确定是否更新component版本
     *  6.是否要将jar包传到公司内网才行。
     * @return
     */
    public List<User> getAll5();
}
